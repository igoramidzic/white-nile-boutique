import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./components/pages/home/home.component";
import { ShopOnlineComponent } from "./components/pages/shop-online/shop-online.component";
import { ContactComponent } from "./components/pages/contact/contact.component";
import { InventoryComponent } from "./components/pages/shop-online/inventory/inventory.component";
import { IndividualItemComponent } from "./components/pages/shop-online/individual-item/individual-item.component";
import { CartComponent } from "./components/pages/cart/cart.component";
import { AccountComponent } from "./components/pages/account/account.component";
import { FavoritesComponent } from "./components/pages/favorites/favorites.component";

// Routes
const appRoutes: Routes = [
  { path: '', component: HomeComponent},
  { path: 'cart', component: CartComponent},
	{ path: 'shop', component: ShopOnlineComponent, children: [
		{ path: '', component: InventoryComponent },
	] },
	{ path: 'shop/item', component: IndividualItemComponent },
  { path: 'contact', component: ContactComponent },
  { path: 'account', component: AccountComponent},
  { path: 'favorites', component: FavoritesComponent}
];

@NgModule({
	imports: [
		RouterModule.forRoot(appRoutes)
	],
	exports: [ RouterModule ]
})
export class AppRoutingModule {

}
