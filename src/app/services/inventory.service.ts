import { Injectable } from '@angular/core';
import { Item } from '../models/item.model';
import { Router, ActivatedRoute } from '@angular/router';
import { SlugifyPipe } from 'ngx-pipes';
import { Subject } from 'rxjs/Subject';
import { AngularFirestore } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class InventoryService {

	mainInventory: Observable<Item[]>;

	constructor(
              private router: Router,
              private route: ActivatedRoute,
              private slugifyPipe: SlugifyPipe,
              private afs: AngularFirestore) {

    this.mainInventory = this.afs.collection('items')
      .snapshotChanges()
      .map(actions => {
        return actions.map(a => {
          const data = a.payload.doc.data() as Item;
          const id = a.payload.doc.id;
          return { id, ...data };
        })
      });
	}

	navToItem (item) {
		let itemUrl;

		this.router.navigate(
			['shop', 'item'],
			{ queryParams: { name: this.slugifyPipe.transform(item.name), id: item.id } }
		);
	}

}





















// { id: '1', name: 'Mini Skirt Sandra', img: '1', category: 'cover-up', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '2', name: 'Elena Dress', img: '2', category: 'cover-up', price: 46.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '3', name: 'Tiffany Top', img: '3', category: 'cover-up', price: 45.99, gender: 'female', sizes: [], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '4', name: 'Nina\'s Shorts', img: '4', category: 'cover-up', price: 62.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '5', name: 'Camila Pants', img: '5', category: 'cover-up', price: 41.99, gender: 'female', sizes: ['M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '6', name: 'Blusa Palma', img: '6', category: 'dresses', price: 35.99, gender: 'female', sizes: ['S', 'M', 'L'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '7', name: 'Andrew Shirt', img: '7', category: 'dresses', price: 49.99, gender: 'male', sizes: ['S', 'M', 'L', 'XL', 'XXL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '8', name: 'Mini Skirt Sandra', img: '1', category: 'dresses', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '9', name: 'Elena Dress', img: '2', category: 'dresses', price: 46.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '10', name: 'Tiffany Top', img: '3', category: 'dresses', price: 45.99, gender: 'female', sizes: [], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '11', name: 'Nina\'s Shorts', img: '4', category: 'tops', price: 62.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '12', name: 'Camila Pants', img: '5', category: 'tops', price: 41.99, gender: 'female', sizes: ['M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '13', name: 'Blusa Palma', img: '6', category: 'tops', price: 35.99, gender: 'female', sizes: ['S', 'M', 'L'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '14', name: 'Andrew Shirt', img: '7', category: 'tops', price: 49.99, gender: 'male', sizes: ['S', 'M', 'L', 'XL', 'XXL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '15', name: 'Mini Skirt Sandra', img: '1', category: 'tops', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '16', name: 'Elena Dress', img: '2', category: 'jackets', price: 46.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '17', name: 'Tiffany Top', img: '3', category: 'jackets', price: 45.99, gender: 'female', sizes: [], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '18', name: 'Nina\'s Shorts', img: '4', category: 'jackets', price: 62.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '19', name: 'Camila Pants', img: '5', category: 'jackets', price: 41.99, gender: 'female', sizes: ['M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '20', name: 'Blusa Palma', img: '6', category: 'jackets', price: 35.99, gender: 'female', sizes: ['S', 'M', 'L'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '21', name: 'Andrew Shirt', img: '7', category: 'bottoms', price: 49.99, gender: 'male', sizes: ['S', 'M', 'L', 'XL', 'XXL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '22', name: 'Mini Skirt Sandra', img: '1', category: 'bottoms', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '23', name: 'Elena Dress', img: '2', category: 'bottoms', price: 46.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '24', name: 'Tiffany Top', img: '3', category: 'bottoms', price: 45.99, gender: 'female', sizes: [], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '25', name: 'Nina\'s Shorts', img: '4', category: 'bottoms', price: 62.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '26', name: 'Camila Pants', img: '5', category: 'jewelry', price: 41.99, gender: 'female', sizes: ['M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '27', name: 'Blusa Palma', img: '6', category: 'jewelry', price: 35.99, gender: 'female', sizes: ['S', 'M', 'L'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '28', name: 'Andrew Shirt', img: '7', category: 'jewelry', price: 49.99, gender: 'male', sizes: ['S', 'M', 'L', 'XL', 'XXL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '29', name: 'Mini Skirt Sandra', img: '1', category: 'jewelry', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '30', name: 'Elena Dress', img: '2', category: 'jewelry', price: 46.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '31', name: 'Tiffany Top', img: '3', category: 'sale', price: 45.99, gender: 'female', sizes: [], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '32', name: 'Nina\'s Shorts', img: '4', category: 'sale', price: 62.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '33', name: 'Camila Pants', img: '5', category: 'sale', price: 41.99, gender: 'female', sizes: ['M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '34', name: 'Blusa Palma', img: '6', category: 'sale', price: 35.99, gender: 'female', sizes: ['S', 'M', 'L'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '35', name: 'Andrew Shirt', img: '7', category: 'sale', price: 49.99, gender: 'male', sizes: ['S', 'M', 'L', 'XL', 'XXL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '36', name: 'Mini Skirt Sandra', img: '1', category: 'cover-up', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '37', name: 'Elena Dress', img: '2', category: 'cover-up', price: 46.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '38', name: 'Tiffany Top', img: '3', category: 'dresses', price: 45.99, gender: 'female', sizes: [], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '39', name: 'Nina\'s Shorts', img: '4', category: 'dresses', price: 62.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '40', name: 'Camila Pants', img: '5', category: 'tops', price: 41.99, gender: 'female', sizes: ['M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '41', name: 'Blusa Palma', img: '6', category: 'tops', price: 35.99, gender: 'female', sizes: ['S', 'M', 'L'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '42', name: 'Andrew Shirt', img: '7', category: 'jackets', price: 49.99, gender: 'male', sizes: ['S', 'M', 'L', 'XL', 'XXL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '43', name: 'Mini Skirt Sandra', img: '1', category: 'jackets', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '44', name: 'Mini Skirt Sandra', img: '1', category: 'bottoms', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
// { id: '45', name: 'Elena Dress', img: '2', category: 'bottoms', price: 46.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '46', name: 'Tiffany Top', img: '3', category: 'jewelry', price: 45.99, gender: 'female', sizes: [], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '47', name: 'Nina\'s Shorts', img: '4', category: 'jewelry', price: 62.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '48', name: 'Camila Pants', img: '5', category: 'sale', price: 41.99, gender: 'female', sizes: ['M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '49', name: 'Blusa Palma', img: '6', category: 'sale', price: 35.99, gender: 'female', sizes: ['S', 'M', 'L'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '50', name: 'Andrew Shirt', img: '7', category: 'bottoms', price: 49.99, gender: 'male', sizes: ['S', 'M', 'L', 'XL', 'XXL'], details: '100% cotton imported from Peru.', reviews: [{ rating: 5, description: 'Awesome piece of clothing!' }] },
// { id: '51', name: 'Mini Skirt Sandra', img: '1', category: 'bottoms', price: 56.99, gender: 'female', sizes: ['S', 'M', 'L', 'XL'], details: '100% cotton imported from Peru.', reviews: [] },
