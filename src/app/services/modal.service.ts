import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { QuickPreviewItemComponent } from '../components/modals/quick-preview-item/quick-preview-item.component';
import { SignupModalComponent } from '../components/modals/signup-modal/signup-modal.component';
import { LoginModalComponent } from '../components/modals/login-modal/login-modal.component';

@Injectable()
export class ModalService {

	constructor(private myModals: MatDialog) { }

	openSignupModal () {
		const quickPreviewItemModalRef = this.myModals.open(SignupModalComponent, {
			disableClose: false
		});

		return quickPreviewItemModalRef;
	}

	openLoginModal () {
		const quickPreviewItemModalRef = this.myModals.open(LoginModalComponent, {
			disableClose: false
		});

		return quickPreviewItemModalRef;
	}

	openQuickPreviewItemModal (item) {
		const quickPreviewItemModalRef = this.myModals.open(QuickPreviewItemComponent, {
			disableClose: false,
			data: { item }
		});

		return quickPreviewItemModalRef;
	}
	
}
