import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';

@Injectable()
export class AuthService {

  constructor(public afAuth: AngularFireAuth) { }

  signupUserWithEmail (email: string, password: string) {
    return new Promise((resolve, reject) => {
			this.afAuth.auth.createUserWithEmailAndPassword(email, password)
				.then(success => resolve(success)).catch(error => reject(error))
		});
  }

  loginUserWithEmail (email: string, password: string) {
    return new Promise((resolve, reject) => {
			this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then(success => resolve(success)).catch(error => reject(error))
		})
  }

  loginUserWithGoogle () {
    return new Promise((resolve, reject) => {
			this.afAuth.auth.signInWithPopup(new firebase.auth.GoogleAuthProvider())
				.then(success => resolve()).catch(error => reject(error));
		})
  }

  loginUserWithFacebook () {
    return new Promise((resolve, reject) => {
			this.afAuth.auth.signInWithPopup(new firebase.auth.FacebookAuthProvider())
				.then(success => resolve()).catch(error => reject(error));
		})
  }

  logout () {
    this.afAuth.auth.signOut();
  }

}
