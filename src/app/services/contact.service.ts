import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class ContactService {

  slackURL: string = 'https://hooks.slack.com/services/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';

  constructor(private http: HttpClient) { }

  sendMessage (message) {
    return this.http.post(this.slackURL, JSON.stringify(message));
  }

}
