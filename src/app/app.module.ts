import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { environment } from '../environments/environment';

import { NgPipesModule, SlugifyPipe } from 'ngx-pipes';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatDialogModule } from '@angular/material/dialog';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatInputModule } from '@angular/material/input';
import { MatCheckboxModule } from '@angular/material/checkbox';

import { ClipboardModule } from 'ngx-clipboard';
import { SimpleNotificationsModule } from 'angular2-notifications';

import { ModalService } from './services/modal.service';
import { InventoryService } from './services/inventory.service';
import { AuthService } from './services/auth.service';
import { ContactService } from './services/contact.service';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/pages/home/home.component';
import { ShopOnlineComponent } from './components/pages/shop-online/shop-online.component';
import { ContactComponent } from './components/pages/contact/contact.component';
import { InventoryComponent } from './components/pages/shop-online/inventory/inventory.component';
import { SidebarComponent } from './components/pages/shop-online/sidebar/sidebar.component';
import { ItemCardComponent } from './components/pages/shop-online/inventory/item-card/item-card.component';
import { IndividualItemComponent } from './components/pages/shop-online/individual-item/individual-item.component';
import { QuickPreviewItemComponent } from './components/modals/quick-preview-item/quick-preview-item.component';
import { SignupModalComponent } from './components/modals/signup-modal/signup-modal.component';
import { LoginModalComponent } from './components/modals/login-modal/login-modal.component';
import { IteratePipe } from './pipes/iterate.pipe';
import { FooterComponent } from './components/footer/footer.component';
import { ReviewsComponent } from './components/pages/shop-online/individual-item/reviews/reviews.component';
import { ShareComponent } from './components/pages/shop-online/individual-item/share/share.component';
import { SimpleNotificationsComponent } from './components/misc/simple-notifications/simple-notifications.component';
import { MainItemInfoComponent } from './components/pages/shop-online/individual-item/main-item-info/main-item-info.component';
import { CartComponent } from './components/pages/cart/cart.component';
import { CheckmarkComponent } from './components/animations/checkmark/checkmark.component';
import { AccountComponent } from './components/pages/account/account.component';
import { FavoritesComponent } from './components/pages/favorites/favorites.component';
import { SpinnerComponent } from './components/animations/spinner/spinner.component';

@NgModule({
	declarations: [
		AppComponent,
		NavbarComponent,
		HomeComponent,
		ShopOnlineComponent,
		ContactComponent,
		InventoryComponent,
		SidebarComponent,
		ItemCardComponent,
		IndividualItemComponent,
		QuickPreviewItemComponent,
		SignupModalComponent,
		LoginModalComponent,
		IteratePipe,
		FooterComponent,
		ReviewsComponent,
    ShareComponent,
    SimpleNotificationsComponent,
    MainItemInfoComponent,
    CartComponent,
    CheckmarkComponent,
    AccountComponent,
    FavoritesComponent,
    SpinnerComponent
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		NgPipesModule,
		BrowserAnimationsModule,
		MatButtonModule,
		MatCardModule,
		MatDialogModule,
    MatTooltipModule,
    ClipboardModule,
    SimpleNotificationsModule.forRoot(),
    AngularFireModule.initializeApp(environment.firebase),
		AngularFirestoreModule,
    AngularFireAuthModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    HttpClientModule
	],
	entryComponents: [
		QuickPreviewItemComponent,
		SignupModalComponent,
		LoginModalComponent
	],
	providers: [InventoryService, ModalService, SlugifyPipe, AuthService, ContactService],
	bootstrap: [AppComponent]
})
export class AppModule { }
