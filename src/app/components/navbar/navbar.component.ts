import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ModalService } from '../../services/modal.service';
import { AuthService } from '../../services/auth.service';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.sass']
})
export class NavbarComponent implements OnInit {

  user: Object;

	constructor(
							public router: Router,
              private modalService: ModalService,
              private authService: AuthService) { }

	ngOnInit() {
    this.authService.afAuth.authState.subscribe(user => this.user = user);
	}

	onOpenSignupModal () {
    this.modalService.openSignupModal()
      .afterClosed().subscribe(res => {
        if (res === "loginModal") {
          this.onOpenLoginModal();
        }
      })
	}

	onOpenLoginModal () {
    this.modalService.openLoginModal()
      .afterClosed().subscribe(res => {
        if (res === "signupModal") {
          this.onOpenSignupModal();
        }
      })
	}

	onNavToShop (category, gender) {
		this.router.navigate(['/shop'], { queryParams: { category, gender }});
  }

  onNavToAccount () {
    this.router.navigate(['account']);
  }

  onLogout () {
    this.authService.logout();
  }

}
