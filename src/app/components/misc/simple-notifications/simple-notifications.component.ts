import { Component, OnInit } from '@angular/core';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-simple-notifications',
  template: '<simple-notifications [options]="options"></simple-notifications>'
})
export class SimpleNotificationsComponent implements OnInit {

  options: any;

  constructor( private notificationsService: NotificationsService ) {}

  ngOnInit() {
    this.options = {
      position: ["bottom", "right"],
      timeOut: 2000,
      lastOnBottom: true,
      showProgressBar: false
    }
  }

}
