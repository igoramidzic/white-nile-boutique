import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { LoginModalComponent } from '../login-modal/login-modal.component';
import { MatDialog } from '@angular/material';

@Component({
  selector: 'app-signup-modal',
  templateUrl: './signup-modal.component.html',
  styleUrls: ['./signup-modal.component.sass']
})
export class SignupModalComponent implements OnInit {

  signupForm: FormGroup;
	complete: boolean;
	signupWithEmailError: string;
	signupWithSocialError: string;
	hasEmailError: boolean;
	hasPasswordError: boolean;
	submitting: boolean;

  constructor(private authService: AuthService,
              private myModal: MatDialog) { }

  ngOnInit() {
    this.signupForm = new FormGroup({
			'email': new FormControl(null),
			'password': new FormControl(null)
		})
		this.complete = false;
  }

  successfulSignup () {
		this.complete = true;
		setTimeout(() => {
			this.signupForm.reset();
			this.myModal.closeAll();
		}, 1000)
	}

	resetSignupErrors () {
		this.signupWithEmailError = null;
		this.signupWithSocialError = null;
		this.hasEmailError = null;
		this.hasPasswordError = null;
	}

	onSignupWithEmail () {
		this.resetSignupErrors();

		var email = this.signupForm.get('email');
		var password = this.signupForm.get('password');

		if (!email.value) {
			this.hasEmailError = true;
		}
		if (!password.value) {
			this.hasPasswordError = true;
		}

		if ( email.value && password.value) {
			this.submitting = true;
			this.authService.signupUserWithEmail(email.value, password.value)
				.then(
					res => {
						this.submitting = null;
						this.successfulSignup();
					}
				)
				.catch(
					error => {
						this.submitting = null;
						if (error.code === "auth/email-already-in-use") {
							this.signupWithEmailError = "This email address has already been used.";
							this.hasEmailError = true;
							password.patchValue('');
						} else if (error.code === "auth/invalid-email") {
							this.signupWithEmailError = "Please enter a valid email.";
							this.hasEmailError = true;
							password.patchValue('');
						} else if (error.code === "auth/weak-password") {
							this.signupWithEmailError = "Password must be at least 6 characters.";
							this.hasPasswordError = true;
							password.patchValue('');
						} else {
							this.signupWithEmailError = "Something went wrong. Please try again.";
						}
					}
				)
		}
	}

	onSignupWithGoogle () {
		this.resetSignupErrors();
		this.authService.loginUserWithGoogle()
			.then(
				res => {
					this.successfulSignup();
				}
			).catch(
				error => {
					if (error.code === "auth/account-exists-with-different-credential") {
						this.signupWithSocialError = "An account associated with this email already exists. Please use a different method."
					} else {
						console.log(error)
					}
				}
			);
	}

	onSignupWithFacebook () {
		this.resetSignupErrors();
		this.authService.loginUserWithFacebook()
			.then(
				res => {
					this.successfulSignup();
				}
			).catch(
				error => {
					if (error.code === "auth/account-exists-with-different-credential") {
						this.signupWithSocialError = "An account associated with this email already exists. Please use a different method."
					}
				}
			);
	}

}
