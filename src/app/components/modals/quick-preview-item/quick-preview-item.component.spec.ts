import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuickPreviewItemComponent } from './quick-preview-item.component';

describe('QuickPreviewItemComponent', () => {
  let component: QuickPreviewItemComponent;
  let fixture: ComponentFixture<QuickPreviewItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuickPreviewItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuickPreviewItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
