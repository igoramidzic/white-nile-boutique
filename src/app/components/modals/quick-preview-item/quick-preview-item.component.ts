import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
	selector: 'app-quick-preview-item',
	templateUrl: './quick-preview-item.component.html',
	styleUrls: ['./quick-preview-item.component.sass']
})
export class QuickPreviewItemComponent implements OnInit {

	constructor(
		public dialogRef: MatDialogRef<QuickPreviewItemComponent>,
		@Inject(MAT_DIALOG_DATA) public data: any
	) { }
		
	ngOnInit() {
	}

}
