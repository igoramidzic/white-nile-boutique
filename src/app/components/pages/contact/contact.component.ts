import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../../services/auth.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import { ContactService } from '../../../services/contact.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.sass']
})
export class ContactComponent implements OnInit {

  user: Object;
  contactForm: FormGroup;
  contactTypes: string[];
  slackMessageObj: SlackMessageObj;
  submitted: boolean = false;

  constructor(private authService: AuthService,
              private contactService: ContactService) { }

  ngOnInit() {
    this.authService.afAuth.authState.subscribe(user => {
      this.user = user;
      if (this.contactForm.controls['email'].value == null && user) {
        this.contactForm.controls['email'].patchValue(user.email);
      }
    });

    this.contactForm = new FormGroup({
      'name': new FormControl(null, [
        Validators.required
      ]),
      'email': new FormControl(null, [
        Validators.required
      ]),
      'type': new FormControl(null, [
        Validators.required
      ]),
      'message': new FormControl(null, [
        Validators.required
      ])
    });

    this.contactTypes = ['Order related', 'Product related', 'Store related'];
  }

  onSubmitContactRequest () {
    if (this.contactForm.valid) {
      this.configureSlackMessageObj();
      this.contactService.sendMessage(this.slackMessageObj).subscribe(
        success => {
          this.contactForm.reset();
          this.submitted = true;
        },
        error => {
          if (error.error.error.message === 'Unexpected token o in JSON at position 0') {
            this.contactForm.reset();
            this.submitted = true;
            return;
          }
        }
      )
    }
  }

  configureSlackMessageObj () {
    let name = this.contactForm.controls['name'].value;
    let email = this.contactForm.controls['email'].value;
    let supportType = this.contactForm.controls['type'].value;
    let message = this.contactForm.controls['message'].value;

    let fullMessage = `New contact request\n
			*Name:* ${name}\n
			*Email:* ${email}\n
			*Support type:* ${supportType}\n
			*Message:* ${message}`;

    this.slackMessageObj = {
      text: fullMessage
    }
  }

}

interface SlackMessageObj {
  text: string
}
