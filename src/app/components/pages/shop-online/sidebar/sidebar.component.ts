import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SlugifyPipe } from 'ngx-pipes';
import { InventoryService } from '../../../../services/inventory.service';
import { AuthService } from '../../../../services/auth.service';

@Component({
	selector: 'app-sidebar',
	templateUrl: './sidebar.component.html',
	styleUrls: ['./sidebar.component.sass'],
	providers: [SlugifyPipe]
})
export class SidebarComponent implements OnInit {

	@Input('categories') categories: string[];
	@Input('genders') genders: string[];
	category: string;
  gender: string;
  user: any = null;

	constructor(
							public route: ActivatedRoute,
							private router: Router,
              private slugifyPipe: SlugifyPipe,
              private authService: AuthService) { }

	ngOnInit() {
		this.category = this.route.snapshot.queryParams['category'];
		this.gender = this.route.snapshot.queryParams['gender'];
		this.route.queryParams
			.subscribe(queryParams => {
				this.category = queryParams['category'];
        this.gender = queryParams['gender'];
        this.authService.afAuth.authState.subscribe(user => {
          this.user = user;
          this.blockAccessToFavoritesIfNoUser();
        })
      });
  }

  blockAccessToFavoritesIfNoUser () {
    if (!this.user && this.route.snapshot.queryParams['category'] === 'favorites') {
      this.onChangeCategory(null);
    }
  }

	onChangeCategory (category: string) {
		let categorySlug = this.slugifyPipe.transform(category);
		this.router.navigate(['/shop'], { queryParams: { category: categorySlug, gender: this.gender }})
	}

	onChangeGender (gender: string) {
		this.router.navigate(['/shop'], { queryParams: { category: this.category, gender }})
	}

}
