import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IndividualItemComponent } from './individual-item.component';

describe('IndividualItemComponent', () => {
  let component: IndividualItemComponent;
  let fixture: ComponentFixture<IndividualItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IndividualItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IndividualItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
