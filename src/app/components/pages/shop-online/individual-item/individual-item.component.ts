import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { InventoryService } from '../../../../services/inventory.service';
import { Item } from '../../../../models/item.model';
import { ModalService } from '../../../../services/modal.service';
import { Location } from '@angular/common';
import { NotificationsService } from 'angular2-notifications';
import { Subscription } from 'rxjs/Subscription';

@Component({
	selector: 'app-individual-item',
	templateUrl: './individual-item.component.html',
	styleUrls: ['./individual-item.component.sass']
})
export class IndividualItemComponent implements OnInit, OnDestroy {

	item: Item;
	mainInventory: Item[];
	categoryInventory: Item[];
  inventorySubscription: Subscription;

  constructor(
              private route: ActivatedRoute,
              private router: Router,
              private inventoryService: InventoryService,
              private notificationsService: NotificationsService) { }

	ngOnInit() {

    // Set this.item to default loading values so page doesn't jump after the real item loads.
    this.item = {
      id: 0,
      name: 'Loading',
      img: '0',
      category: '',
      price: 0.00,
      gender: null,
      sizes: ['-', '-', '-', '-'],
      details: 'Details',
      reviews: []
    }

    this.inventorySubscription = this.inventoryService.mainInventory.subscribe(inventory => {
      this.mainInventory = inventory

      this.route.queryParams.subscribe(params => {
        // Set this.item to item mathing queryParam 'id'
        this.item = this.mainInventory.filter(item => {
          return item.id === this.route.snapshot.queryParams['id'];
        })[0] // Grab the first of the array
        this.updateRecommendedInventory(this.item);
      })
    })
  }

  ngOnDestroy () {
    this.inventorySubscription.unsubscribe();
  }

	updateRecommendedInventory (item) {
		this.categoryInventory = this.mainInventory.filter(obj => {
			return (obj.category === item.category) && (obj.gender === item.gender) && (obj.id !== item.id);
		})
  }

	onNavToItem (item) {
		this.inventoryService.navToItem(item);
		this.updateRecommendedInventory(item);
  }
}
