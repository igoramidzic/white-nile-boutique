import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../../../models/item.model';
import { ModalService } from '../../../../../services/modal.service';

@Component({
  selector: 'app-reviews',
  templateUrl: './reviews.component.html',
  styleUrls: ['./reviews.component.sass']
})
export class ReviewsComponent implements OnInit {

  @Input('item') item: Item;

  constructor(private modalService: ModalService) { }

  ngOnInit() {
  }

  onWriteAReview () {
		this.modalService.openLoginModal();
	}

}
