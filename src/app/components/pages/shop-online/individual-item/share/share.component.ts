import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../../../models/item.model';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-share',
  templateUrl: './share.component.html',
  styleUrls: ['./share.component.sass']
})
export class ShareComponent implements OnInit {

  @Input('item') item: Item;
  itemLink: string;
  copied: boolean = false;

  constructor(private router: Router,
              private notificationsService: NotificationsService) { }

  ngOnInit() {
    this.itemLink = window.location.href;
  }

  onCopy () {
    this.notificationsService.success('Copied', 'to clipboard', {timeOut: 1500,})
  }

}
