import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../../../models/item.model';
import { NotificationsService } from 'angular2-notifications';
import { MatDialog } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-main-item-info',
  templateUrl: './main-item-info.component.html',
  styleUrls: ['./main-item-info.component.sass']
})
export class MainItemInfoComponent implements OnInit {

  @Input('item') item: Item;
  size: string;
  get avgRating (): number {
		if (this.item.reviews.length) {
			return Math.round(this.item.reviews.reduce( ( p, c ) => p + c.rating, 0 ) / this.item.reviews.length);
		}
		return 0;
	}

  constructor(private notificationsService: NotificationsService,
              private myModal: MatDialog,
              private router: Router,
              private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.queryParams.subscribe((query) => {
			if (!this.item.sizes.length) {
				this.size = 'One size';
			}
		})
  }

  addToFavorites() {
	}

	addToCart () {
    // After setting item in cart:
    this.myModal.closeAll();
    this.router.navigate(['cart']);
    this.notificationsService.success('Added to cart', this.item.name, {timeOut: 3000});
	}

}
