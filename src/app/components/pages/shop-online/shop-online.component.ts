import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
	selector: 'app-shop-online',
	templateUrl: './shop-online.component.html',
	styleUrls: ['./shop-online.component.sass']
})
export class ShopOnlineComponent implements OnInit {

	categories: String[];
	genders: string[];

	constructor() { }

	ngOnInit() {
		this.categories = [
			'cover up',
			'dresses',
			'tops',
			'jackets',
			'bottoms',
			'jewelry',
			'sale'
		];
		
		this.genders = [
			'male',
			'female'
		]
	}

}
