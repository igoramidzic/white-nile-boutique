import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../../../../models/item.model';
import { Router } from '@angular/router';
import { ModalService } from '../../../../../services/modal.service';
import { SlugifyPipe } from 'ngx-pipes';
import { InventoryService } from '../../../../../services/inventory.service';

@Component({
	selector: 'app-item-card',
	templateUrl: './item-card.component.html',
	styleUrls: ['./item-card.component.sass'],
	providers: [SlugifyPipe]
})
export class ItemCardComponent implements OnInit {

	@Input('item') item: Item;

	constructor(
							private router: Router,
							private modalService: ModalService,
							private slugifyPipe: SlugifyPipe,
							private inventoryService: InventoryService) { }

	ngOnInit() {
	}

	openQuickPreviewItemModal () {
		this.modalService.openQuickPreviewItemModal(this.item);
	}

	onNavToItem () {
		this.inventoryService.navToItem(this.item);
	}

}
