import { Component, OnInit, Input, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Item } from '../../../../models/item.model';
import { InventoryService } from '../../../../services/inventory.service';
import { Subscription } from 'rxjs/Subscription';

@Component({
	selector: 'app-inventory',
	templateUrl: './inventory.component.html',
	styleUrls: ['./inventory.component.sass']
})
export class InventoryComponent implements OnInit, OnDestroy {

  mainInventory: Item[];
  filteredInventory: Item[];
  inventorySubscription: Subscription;

	constructor(private inventoryService: InventoryService,
							private route: ActivatedRoute) { }

	ngOnInit() {
    this.inventorySubscription = this.inventoryService.mainInventory.subscribe(inventory => {
      this.mainInventory = inventory;

      this.route.queryParams.subscribe(params => {
        this.filterInventory(params['category'], params['gender']);
      })
    })
  }

  ngOnDestroy () {
    this.inventorySubscription.unsubscribe();
  }

  filterInventory (category, gender) {
    this.filteredInventory = this.mainInventory
      .filter(item => {
        if (category) {
          return item.category === category;
        }
        return true;
      })
      .filter(item => {
        if (gender) {
          return item.gender === gender;
        }
        return true;
      });
	}

}
