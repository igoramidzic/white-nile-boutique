import { Component, OnInit } from '@angular/core';

@Component({
	selector: 'app-footer',
	templateUrl: './footer.component.html',
	styleUrls: ['./footer.component.sass']
})
export class FooterComponent implements OnInit {

	constructor() { }

	ngOnInit() {
	}

	onContact () {
    console.log("Bring to contact page OR open modal");
	}

	onMoreInformation () {
    console.log("Bring to information page OR open modal");
	}

	onSomething () {
		console.log("Indetermined action");
	}

}
