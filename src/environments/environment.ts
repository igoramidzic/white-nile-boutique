// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
		apiKey: "AIzaSyAZrvungyORK22Y18oMoGzPCDGCYwr79nI",
    authDomain: "white-nile.firebaseapp.com",
    databaseURL: "https://white-nile.firebaseio.com",
    projectId: "white-nile",
    storageBucket: "white-nile.appspot.com",
    messagingSenderId: "17066298725"
	}
};
